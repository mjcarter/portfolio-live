[![Netlify Status](https://api.netlify.com/api/v1/badges/0189823d-43e8-43e1-8bd6-f06965cc625f/deploy-status)](https://app.netlify.com/sites/mcarter-portfolio-live/deploys)


**Start developing.**

Navigate into your new site’s directory and start it up.

```shell
cd portfolio-live/
gatsby develop
```
