import React from "react"
import {Link} from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

const IndexPage = () => (
    <Layout>
        <SEO title="Home"/>
        <p>The site is currently under construction! I've listed important links below in the meantime.</p>
        <ul>
            <li>
                <a href="https://www.gitlab.com/mjcarter">Gitlab</a>
            </li>
            <li>
                <a href="https://www.github.com/mjc3bb">GitHub</a>
            </li>
            <li>
                <a href="https://www.linkedin.com/in/michaeljcarter3/">LinkedIn</a>
            </li>
            <li>
                <a href="mailto:mjc3bb@gmail.com">Email me</a>
            </li>
        </ul>
    </Layout>
)

export default IndexPage
